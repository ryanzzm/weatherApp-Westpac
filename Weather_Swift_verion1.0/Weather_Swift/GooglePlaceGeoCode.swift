//
//  GooglePlaceGeoCode.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 6/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import Foundation

class GooglePlaceGeoCode:GooglePlaceGeoCodeObject{
    var results:[Results]?
    var status:String?
    
    override func setValue(value: AnyObject?, forKey key: String){
        if key == "results"{
            results = [Results]()
            let valueArr = value as! [[String: AnyObject]]
            
            for values in valueArr{
                let dataValue = Results()
                dataValue.setValuesForKeysWithDictionary(values)
                results?.append(dataValue)
            }
        }
        else{
            super.setValue(value, forKey: key)
        }
    }
}

class Results:GooglePlaceGeoCodeObject{
    var address_components: AnyObject?
    var formatted_address: String?
    var geometry: Geometry?
    var place_id: String?
    var types: AnyObject?
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "geometry"{
            geometry = Geometry()
            geometry?.setValuesForKeysWithDictionary(value as! [String : AnyObject])
        }
        else{
            super.setValue(value, forKey: key)
        }
    }
}

class Geometry:GooglePlaceGeoCodeObject{
    
    var bounds:AnyObject?
    var location:GeoLocation?
    var location_type:AnyObject?
    var viewport:AnyObject?
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "location"{
            location = GeoLocation()
            location?.setValuesForKeysWithDictionary(value as! [String : AnyObject])
        }
        else{
            super.setValue(value, forKey: key)
        }
    }
    
}

class GeoLocation:GooglePlaceGeoCodeObject{
    var lat:AnyObject?
    var lng:AnyObject?
    
}


class GooglePlaceGeoCodeObject:NSObject{
    override func setValue(value: AnyObject?, forKey key: String) {
        let selectorString = "set\(key.uppercaseString.characters.first!)\(String(key.characters.dropFirst())):"
        let selector = Selector(selectorString)
        if respondsToSelector(selector){
            super.setValue(value, forKey: key)
        }
    }
    
}

class GooglePlaceGeocodeServcie{
    
    var Lat: AnyObject?
    var Lng: AnyObject?
    func getCityGeocode(cityName:String) -> (lat: AnyObject, lng: AnyObject){
        
        if let GooglePlaceApiURL = NSURL(string: "https://maps.googleapis.com/maps/api/geocode/json?address=\(cityName)"){
            
            do {
                
                let jsonData = try(NSData(contentsOfURL: GooglePlaceApiURL, options: NSDataReadingOptions.DataReadingMappedIfSafe))
                let jsonDic = try(NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers))
                
                
                if let GooglePlaceDic = jsonDic as? [String:AnyObject]{
                    let GooglePlace = GooglePlaceGeoCode()
                    GooglePlace.setValuesForKeysWithDictionary(GooglePlaceDic)
                    
                    Lat = GooglePlace.results![0].geometry?.location?.lat
                    Lng = GooglePlace.results![0].geometry?.location?.lng

                }
            }
                
            catch let err{
                print(err)
            }
        }
        return (Lat!, lng: Lng!)
    }
    
    
    
    
}