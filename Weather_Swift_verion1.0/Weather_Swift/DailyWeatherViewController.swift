//
//  DailyWeatherViewController.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 28/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//


import UIKit

class DailyWeatherViewController: UIViewController {

    @IBOutlet weak var dailyTime: UILabel!
    @IBOutlet weak var dailyIcon: UIImageView!
    @IBOutlet weak var dailySum: UILabel!
    @IBOutlet weak var dailySunrise: UILabel!
    @IBOutlet weak var dailySunset: UILabel!
    @IBOutlet weak var dailyMaxTemp: UILabel!
    @IBOutlet weak var dailyMinTemp: UILabel!
    @IBOutlet weak var dailyHum: UILabel!
    @IBOutlet weak var dailyRain: UILabel!
    @IBOutlet weak var dailyWind: UILabel!
    @IBOutlet weak var dailyPressure: UILabel!
    
    var aDailyTime: String?
    var aDailyIcon:String?
    var aDailySum:String?
    var aDailySunrise:String?
    var aDailySunset:String?
    var aDailyMixT:String?
    var aDailyMinT:String?
    var aDailyHum:String?
    var aDailyWind:String?
    var aDailyPrecip:String?	
    var aDailyPressure:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        dailyTime.text = aDailyTime!
        dailyHum.text = aDailyHum!
        dailySum.text = aDailySum
        dailyRain.text = aDailyPrecip!
        dailyWind.text = aDailyWind!
        dailySunset.text = aDailySunset!
        dailySunrise.text = aDailySunrise!
        dailyMaxTemp.text = aDailyMixT!
        dailyMinTemp.text = aDailyMinT!
        dailyPressure.text = aDailyPressure!
        dailyIcon.image = UIImage(named: aDailyIcon!)
        
    }
    
    
}
