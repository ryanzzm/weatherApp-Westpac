//
//  ViewController.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 31/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import UIKit

class ViewController: UIViewController, ForecastDelegate {
    
    var forecastService = ForecastService()
    var googlePlaceGeoCodeService = GooglePlaceGeocodeServcie()
    
    var forecast: Forecast?
    
    var toPass:Double?

    @IBOutlet weak var currentTemp: UILabel!
    @IBAction func selectButton(sender: UIButton) {
        forecastService.getForecastValue("-33.8684324", lng: "151.1127366")
        self.googlePlaceGeoCodeService.getCityGeocode("Burwood%20NSW")
    }
    
    
    func setForecasts(forecast:Forecast) {
       
        print("testing 123")
        currentTemp.text = forecast.currently?.temperature?.stringValue
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.forecastService.delegate = self
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

