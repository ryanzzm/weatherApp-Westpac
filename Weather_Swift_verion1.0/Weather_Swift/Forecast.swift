//
//  Forecast.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 6/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import Foundation



class Forecast:ForecastJSONObject{
    var latitude:AnyObject?
    var longitude:AnyObject?
    var timezone:String?
    var offset:AnyObject?
    var currently:Currently?
    var hourly:Hourly?
    var daily:Daily?
    var flags:AnyObject?
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "currently"{
            currently = Currently()
            currently?.setValuesForKeysWithDictionary(value as! [String : AnyObject])
        }
        else if key == "hourly"{
            hourly = Hourly()
            hourly?.setValuesForKeysWithDictionary(value as! [String : AnyObject])
        }
        else if key == "daily"{
            daily = Daily()
            daily?.setValuesForKeysWithDictionary(value as! [String: AnyObject])
        }
        else{
            super.setValue(value, forKey: key)
        }
    }
    
}

class Currently:ForecastJSONObject{
    var time:String?
    var summary:String?
    var icon:String?
    var precipIntensity:AnyObject?
    var precipProbability:AnyObject?
    var temperature:AnyObject?
    var apparentTemperature:AnyObject?
    var dewPoint:AnyObject?
    var humidity:AnyObject?
    var windSpeed:AnyObject?
    var windBearing:AnyObject?
    var visibility:AnyObject?
    var cloudCover:AnyObject?
    var pressure:AnyObject?
    var ozone:AnyObject?
    
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "time"{
            time = ForecastService().convertUNIXtimeToFull((value?.doubleValue)!)
        }
        else {
            super.setValue(value, forKey: key)
        }
    }
}


class Hourly:ForecastJSONObject{
    var summary:String?
    var icon:String?
    var data:[hourlyArray]?
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "data"{
            
            data = [hourlyArray]()
            
            let valueArr = value as! [[String: AnyObject]]
            
            for values in valueArr{
                let dataValue = hourlyArray()
                dataValue.setValuesForKeysWithDictionary(values)
                data?.append(dataValue)
            }
            
        }
        else{
            super.setValue(value, forKey: key)
            
        }
    }
    
    
}

class hourlyArray:ForecastJSONObject{
    var time:String?
    var summary:String?
    var icon:String?
    var precipIntensity:AnyObject?
    var precipProbability:AnyObject?
    var temperature:AnyObject?
    var apparentTemperature:AnyObject?
    var dewPoint:AnyObject?
    var humidity:AnyObject?
    var windSpeed:AnyObject?
    var windBearing:AnyObject?
    var visibility:AnyObject?
    var cloudCover:AnyObject?
    var pressure:AnyObject?
    var ozone:AnyObject?
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "time"{
            time = ForecastService().convertUNIXtimeToHours((value?.doubleValue)!)
        }
        else {
            super.setValue(value, forKey: key)
        }
    }
    
    
}


class Daily:ForecastJSONObject{
    var summary:String?
    var icon:String?
    var data: [dailyArray]?
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "data"{
            
            data = [dailyArray]()
            
            let valueArr = value as! [[String: AnyObject]]
            
            for values in valueArr{
                let dataValue = dailyArray()
                dataValue.setValuesForKeysWithDictionary(values)
                data?.append(dataValue)
            }
            
        }
        else{
            super.setValue(value, forKey: key)
            
        }
    }
}

class dailyArray:ForecastJSONObject{
    var time:String?
    var summary:String?
    var icon:String?
    var sunriseTime:String?
    var sunsetTime:String?
    var moonPhase:AnyObject?
    var precipIntensity:AnyObject?
    var precipIntensityMax:AnyObject?
    var precipIntensityMaxTime:AnyObject?
    var precipProbability:AnyObject?
    var precipType:AnyObject?
    var temperatureMin:AnyObject?
    var temperatureMinTime:AnyObject?
    var temperatureMax:AnyObject?
    var temperatureMaxTime:AnyObject?
    var apparentTemperatureMin:AnyObject?
    var apparentTemperatureMinTime:NSNumber?
    var apparentTemperatureMax:AnyObject?
    var apparentTemperatureMaxTime:AnyObject?
    var dewPoint:AnyObject?
    var humidity:AnyObject?
    var windSpeed:AnyObject?
    var windBearing:AnyObject?
    var cloudCover:AnyObject?
    var pressure:AnyObject?
    var ozone:AnyObject?
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "time"{
            time = ForecastService().convertUNIXtimeToDates((value?.doubleValue)!)
        }
        else if key == "sunsetTime"{
            sunsetTime = ForecastService().convertUNIXtimeToFull((value?.doubleValue)!)
        }
        else if key == "sunriseTime"{
            sunriseTime = ForecastService().convertUNIXtimeToFull((value?.doubleValue)!)
        }
        
        else {
            super.setValue(value, forKey: key)
        }
    }
    
}

class Flags:ForecastJSONObject {
    
    var sources:AnyObject?
    var units:String?
}


class ForecastJSONObject:NSObject{
    override func setValue(value: AnyObject?, forKey key: String) {
        let selectorString = "set\(key.uppercaseString.characters.first!)\(String(key.characters.dropFirst())):"
        let selector = Selector(selectorString)
        if respondsToSelector(selector){
            super.setValue(value, forKey: key)
        }
    }
    
}

//create forecast delegate protocal
protocol ForecastDelegate {
    func setForecasts(forecast: Forecast)
}

class ForecastService{
    
    var delegate: ForecastDelegate?
    
    func getForecastValue(lat:AnyObject,lng:AnyObject){
        
        let forecast = Forecast()
        
//        let url = NSURL(string: "https://api.forecast.io/forecast/5417ce2f3c07bdac7bf5341fa01d4619/\(lat),\(lng)?units=auto")
        let url = NSURL(string: "https://api.forecast.io/forecast/5417ce2f3c07bdac7bf5341fa01d4619/\(lat),\(lng)")
        do{
            let jsonData = try(NSData(contentsOfURL: url!, options: NSDataReadingOptions.DataReadingMappedIfSafe))
            let jsonDic = try(NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers))
            if let forecastDic = jsonDic as? [String: AnyObject]{
                
                //set NSUserDefault Value...
                let userDefault = NSUserDefaults.standardUserDefaults()
                userDefault.setObject(forecastDic, forKey: "forecastItems")
                userDefault.synchronize()
                
                forecast.setValuesForKeysWithDictionary(forecastDic)
                
                delegate?.setForecasts(forecast)
            }
        }
        catch let err{
            print(err)
        }
    }
    
    func convertUNIXtimeToDates (unixTime:Double) -> String{
        
        let date = NSDate(timeIntervalSince1970: unixTime)
        let formatter = NSDateFormatter()
        formatter.dateFormat = "EEE dd/MM/yyyy"
        let dateTime = formatter.stringFromDate(date)
        return dateTime
    }
    func convertUNIXtimeToHours (unixTime:Double) -> String{
        
        let date = NSDate(timeIntervalSince1970: unixTime)
        let formatter = NSDateFormatter()
        formatter.dateFormat = "hha-dd/MM/yyyy"
        let dateTime = formatter.stringFromDate(date)
        return dateTime
    }
    
    func convertUNIXtimeToFull (unixTime:Double) -> String{
        
        let date = NSDate(timeIntervalSince1970: unixTime)
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd/MM/yyyy hh:mm:ss a"
        let dateTime = formatter.stringFromDate(date)
        return dateTime
    }
    
    func convertFashrenheitToCelsius(F:AnyObject) -> Double{
        //display degree symble
        //let temperatureDegree = "\u{00B0}"
        
        //set double digits(10 means 1 digits)
        //let c1 = Double(round(10 * c)/10)

        //convert F to C
        let c = (F.doubleValue - 32)/1.8
        
        return c
    }
    
}



