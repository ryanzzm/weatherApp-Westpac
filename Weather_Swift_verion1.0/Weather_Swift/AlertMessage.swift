//
//  AlertMessage.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 1/09/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import UIKit

class Alert: NSObject {
    
    
    static func show(title: String, message: String, vc: UIViewController){
    
    //create alert controller
        let alertController = UIAlertController(title: title , message: message, preferredStyle: UIAlertControllerStyle.Alert)
    
        
    //create alert action
        let alertBtnOK = UIAlertAction(title: "OK", style: UIAlertActionStyle.Default){
            (alert: UIAlertAction) in
            alertController.dismissViewControllerAnimated(true, completion: nil)
        }
        
        
        //add alert action to alert controller
        alertController.addAction(alertBtnOK)
        
        //display alert controller
        vc.presentViewController(alertController, animated: true, completion: nil)
        
    }
}
