//
//  ViewController.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 6/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import UIKit
import CoreLocation

//

class MainViewController: UIViewController, CLLocationManagerDelegate, ForecastDelegate, UITableViewDataSource, UITableViewDelegate {
    
//    var forecastService = ForecastService(){
//        didSet{
//            self.UIForecastControl()
//        }
//    }

    let userDefaults = NSUserDefaults.standardUserDefaults()
    
    let defaultForecast = Forecast()
    
    let locationManager = CLLocationManager()
    var forecastService = ForecastService()
    var googlePlaceGeoCodeService = GooglePlaceGeocodeServcie()
    
    var dailyInfo:[dailyArray]?
    var hourlyInfo:[hourlyArray]?
    
    var cityName:String?
    var currentLocation:String?
    var cityLat:Double?
    var cityLng:Double?
    var checkFlag = false
    var detailCheckFlag = false
    
   
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView2: UITableView!
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var currentTime: UILabel!
    @IBOutlet weak var btnCity: UIButton!
    
    @IBOutlet weak var currentTemp: UILabel!
    @IBOutlet weak var currentSum: UILabel!
    @IBOutlet weak var currentHum: UILabel!
    @IBOutlet weak var currentWindSpeed: UILabel!
    @IBOutlet weak var currentPressure: UILabel!
    
    @IBOutlet weak var currentIcon: UIImageView!
    
    @IBAction func BtnRefresh(sender: AnyObject) {
        if cityName != nil{
            activityIndicator.startAnimating()
            loadForecast(cityName!)
        }
    }
    
    
    @IBAction func BtncurrentLocation(sender: AnyObject) {
        getGPSLocation()
        
    }
    func setForecasts(forecast:Forecast) {
        
        //set NSUserDefault Value --- City Name
        userDefaults.setObject(cityName, forKey: "defaultCityName")
        userDefaults.synchronize()
        
        //set daily and hourly data
        dailyInfo = forecast.daily?.data
        hourlyInfo = forecast.hourly?.data
        
        //determine daily and hourly data available
        detailCheckFlag = true
        
        //set storyboard data
        self.btnCity.setTitle(cityName, forState: UIControlState.Normal)
        
        self.currentTime.text = forecast.currently?.time
        let tempC = forecastService.convertFashrenheitToCelsius((forecast.currently?.temperature!)!)
        self.currentTemp.text = tempC.fixedDigitsAndDegreeC(1)
        
        self.currentSum.text = forecast.currently?.summary
        
        let hum = (forecast.currently?.humidity?.doubleValue)! * 100
        self.currentHum.text = hum.fixedDigitsAndPersentage(1)
        self.currentWindSpeed.text = forecast.currently?.windSpeed!.doubleValue.fixedDigitsAndSpeed(1)
        self.currentPressure.text = forecast.currently?.pressure?.doubleValue.fixedDigits(1)
        
        self.currentIcon.image = UIImage(named: (forecast.currently?.icon)!)
    
        tableView.reloadData()
        tableView2.reloadData()
        activityIndicator.stopAnimating()
    }
    
    override func viewWillAppear(animated: Bool) {
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.forecastService.delegate = self

        if checkFlag == true{
            loadForecast(cityName!)
        }
            
        ////// retrieving NSUserDefault Values
        else if let loadDefaultData = userDefaults.objectForKey("forecastItems") as? [String: AnyObject]{
            cityName = userDefaults.objectForKey("defaultCityName") as? String
            
            defaultForecast.setValuesForKeysWithDictionary(loadDefaultData)
            setForecasts(defaultForecast)
            
        }
            
            
        //////
        else{
            cityName = "Sydney, New South Wales, Australia"
            loadForecast(cityName!)
            
            if CLLocationManager.locationServicesEnabled() {
                switch(CLLocationManager.authorizationStatus()) {
                case .NotDetermined, .Restricted, .Denied:
                    return
                case .AuthorizedAlways, .AuthorizedWhenInUse:
                    getGPSLocation()
                }
            } else {

            }
            
            
        }
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        tableView2.dataSource = self
        tableView2.delegate = self
        tableView2.registerClass(UITableViewCell.self, forCellReuseIdentifier: "cell1")
        
        
        
        
        // Do any additional setup after loading the view, typically from a nib.

    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        //send VC value to MainVC
        if segue.identifier == "showDailyWeather"{
            
            let DestVC = segue.destinationViewController as! DailyWeatherViewController
            //let DestVC = nav.topViewController as! DailyWeatherViewController
            var indexPath:NSIndexPath
            indexPath = self.tableView.indexPathForSelectedRow!
            let dailyData = dailyInfo![indexPath.row]
           
            //segue data to dailyVC
            DestVC.aDailyTime = dailyData.time!
            
            let hum = (dailyData.humidity?.doubleValue)! * 100
            DestVC.aDailyHum = hum.fixedDigitsAndPersentage(1)
            
            DestVC.aDailyIcon = dailyData.icon!
            DestVC.aDailyPressure = dailyData.pressure?.doubleValue.fixedDigits(1)
            
            let precip = (dailyData.precipProbability?.doubleValue)! * 100
            DestVC.aDailyPrecip = precip.fixedDigitsAndPersentage(1)
            
            DestVC.aDailySum = dailyData.summary!
            DestVC.aDailyMinT = forecastService.convertFashrenheitToCelsius(dailyData.temperatureMin!).fixedDigitsAndDegreeC(1)
            
            DestVC.aDailyMixT = forecastService.convertFashrenheitToCelsius(dailyData.temperatureMax!).fixedDigitsAndDegreeC(1)
            
            DestVC.aDailyWind = dailyData.windSpeed?.doubleValue.fixedDigitsAndSpeed(1)
            DestVC.aDailySunrise = dailyData.sunriseTime!
            DestVC.aDailySunset = dailyData.sunsetTime!
        
            DestVC.aDailyPressure = dailyData.pressure?.stringValue
            
            
            
        }
    }
    
    
    func loadForecast(locationName: String){
        if let cityEscaped = locationName.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet()){
            let GeoCode = GooglePlaceGeocodeServcie().getCityGeocode(cityEscaped)
            cityLat = (GeoCode.lat as? NSNumber)?.doubleValue
            cityLng = (GeoCode.lng as? NSNumber)?.doubleValue
            
            forecastService.getForecastValue(cityLat!, lng: cityLng!)
        }
    
    }

    
    
    func getGPSLocation() {
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .NotDetermined, .Restricted, .Denied:
                Alert.show("Location Service Error", message: "Weather App can't find your current location", vc: self)
            case .AuthorizedAlways, .AuthorizedWhenInUse:
                activityIndicator.startAnimating()
                locationManager.startUpdatingLocation()
            }
        } else {
            Alert.show("Location Service Error", message: "Weather App can't find your current location. Please make sure your location service is Enabled.", vc: self)
        }
        
    }
    
    
    func locationManager(manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
            CLGeocoder().reverseGeocodeLocation(manager.location!) { (placemarks, error) in
            if error != nil{
                print(error)
                self.activityIndicator.stopAnimating()
                return
            }
            else{
                let pm = placemarks![0] as CLPlacemark
                self.displayLocationInfo(pm)
            }
        }
    }
    
    func displayLocationInfo(placemark: CLPlacemark){
        locationManager.stopUpdatingLocation()
        cityName = "\(placemark.locality!), \(placemark.administrativeArea!), \(placemark.country!)"
        loadForecast(cityName!)
    }

    //get new location
//    func locationManager(manager: CLLocationManager, didUpdateToLocation newLocation: CLLocation, fromLocation oldLocation: CLLocation) {
//        locationManager.stopUpdatingLocation()
//    }
    
    
    
    func locationManager(manager: CLLocationManager, didFailWithError error: NSError) {
        //Alert.show("Location Error", message: "Weather App can't find your current location", vc: self)

        //print("location error \(error) \(error.userInfo)")
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var returnValue:Int!
        if (tableView == self.tableView)
        {
            if detailCheckFlag == false{
                returnValue = 0
            }
            else{
                returnValue = dailyInfo!.count
            }
        }
        
        if(tableView == self.tableView2){
            if detailCheckFlag == false{
                returnValue = 0
            }
            else{
                returnValue = hourlyInfo!.count
            }
        }
        return returnValue
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       
        var cell:ForecastDailyCell!
        if(tableView == self.tableView){
            cell = tableView.dequeueReusableCellWithIdentifier("dailyCells", forIndexPath: indexPath) as! ForecastDailyCell
            if detailCheckFlag == true{
                let daily = dailyInfo![indexPath.row]
                
                cell.dailyCellSum.text = daily.summary
                cell.dailyCellTime.text = daily.time
                
                cell.dailyCellTempMax.text = forecastService.convertFashrenheitToCelsius((daily.temperatureMax)!).fixedDigitsAndDegreeC(1)
                cell.dailyCellTempMin.text = forecastService.convertFashrenheitToCelsius((daily.temperatureMin)!).fixedDigitsAndDegreeC(1)
                cell.dailyCellIcon.image = UIImage(named: daily.icon!)
            }
        }
        
        if(tableView == self.tableView2){
            cell = tableView.dequeueReusableCellWithIdentifier("hourlyCells", forIndexPath: indexPath) as! ForecastDailyCell
            if detailCheckFlag == true{
                let hourly = hourlyInfo![indexPath.row]
                cell.hourlyCellTime.text = hourly.time
                
                cell.hourlyCellTemp.text = forecastService.convertFashrenheitToCelsius((hourly.temperature)!).fixedDigitsAndDegreeC(1)
                cell.hourlyCellIcon.image = UIImage(named: hourly.icon!)
               
            }
        }

        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if(tableView == self.tableView){
            
        
        }
        
    }
}


extension Double{
    func fixedDigits(digits: Int) -> String{
        return String(format: "%.\(digits)f", self)
    }
    func fixedDigitsAndDegreeC(digits: Int) ->String{
        return String(format: "%.\(digits)f\u{00B0}C", self)
    }
    func fixedDigitsAndDegreeF(digits: Int) ->String{
        return String(format: "%.\(digits)f\u{00B0}F", self)
    }
    func fixedDigitsAndPersentage(digits: Int) ->String{
        return String(format: "%.\(digits)f\u{FF05}", self)
    }
    func fixedDigitsAndSpeed(digits: Int) ->String{
        return String(format: "%.\(digits)fkm/h", self)
    }
}