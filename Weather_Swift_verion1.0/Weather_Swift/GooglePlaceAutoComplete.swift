//
//  GooglePlaceAutoComplete.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 6/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import Foundation

class GooglePlaceAutoComplete:GooglePlaceAutoCompleteObject{
    var prediction:[Predictions]?
    var status:String?
    
    override func setValue(value: AnyObject?, forKey key: String){
        if key == "predictions"{
            prediction = [Predictions]()
            let valueArr = value as! [[String: AnyObject]]
            
            for values in valueArr{
                let dataValue = Predictions()
                dataValue.setValuesForKeysWithDictionary(values)
                prediction?.append(dataValue)
            }
        }
        else{
            super.setValue(value, forKey: key)
            
        }
        
    }
    
    
}
class Predictions:GooglePlaceAutoCompleteObject{
    var locationDesc: String?
    var id: String?
    var matched_substrings: AnyObject?
    var place_id: String?
    var reference: AnyObject?
    var terms: AnyObject?
    var type: AnyObject?
    
    override func setValue(value: AnyObject?, forKey key: String) {
        if key == "description"{
            super.setValue(value, forKey: "locationDesc")
        }
    }
    
}
class GooglePlaceAutoCompleteObject:NSObject{
    override func setValue(value: AnyObject?, forKey key: String) {
        let selectorString = "set\(key.uppercaseString.characters.first!)\(String(key.characters.dropFirst())):"
        let selector = Selector(selectorString)
        if respondsToSelector(selector){
            super.setValue(value, forKey: key)
        }
    }
    
}

class GooglePlaceAutoCompeletService{
    
    func getCitiesArray(cityName:String) -> [String]{
        
        var locationlist = [String]()
        
        let GooglePlaceApiKey = "AIzaSyDi4S9vNHDOyTAyXP_A_iXMdJdXSy7Sawo"
        
        if let GooglePlaceApiURL = NSURL(string: "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=\(cityName)&types=(cities)&language=en&key=\(GooglePlaceApiKey)"){
            
            do {
                
                let jsonData = try(NSData(contentsOfURL: GooglePlaceApiURL, options: NSDataReadingOptions.DataReadingMappedIfSafe))
                let jsonDic = try(NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers))
                
                
                if let GooglePlaceDic = jsonDic as? [String:AnyObject]{
                    let GooglePlace = GooglePlaceAutoComplete()
                    GooglePlace.setValuesForKeysWithDictionary(GooglePlaceDic)
                    
                    let count = GooglePlace.prediction?.count
                    
                    for i in 0.stride(to: count!, by: 1) {
                        let cities = GooglePlace.prediction![i].locationDesc!
                        locationlist.append(cities)
                    }
                    
                }
            }
                
            catch let err{
                print(err)
            }
            
        }
        return locationlist
        
    }
    
    
    
}


