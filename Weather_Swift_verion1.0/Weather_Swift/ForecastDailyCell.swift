//
//  ForecastDailyCell.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 6/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import UIKit

class ForecastDailyCell: UITableViewCell {
    
    
    @IBOutlet weak var dailyCellSum: UILabel!
 
    @IBOutlet weak var dailyCellTime: UILabel!
    @IBOutlet weak var dailyCellTempMax: UILabel!
    @IBOutlet weak var dailyCellTempMin: UILabel!
    @IBOutlet weak var hourlyCellTime: UILabel!
    
    @IBOutlet weak var dailyCellIcon: UIImageView!
    @IBOutlet weak var hourlyCellIcon: UIImageView!
    @IBOutlet weak var hourlyCellTemp: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

}
