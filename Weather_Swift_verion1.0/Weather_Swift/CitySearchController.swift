//
//  CitySearchController.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 6/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import UIKit
import CoreData

class CitySearchController: UIViewController, UISearchBarDelegate, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var SearchBar: UISearchBar!
    @IBOutlet weak var TableView: UITableView!
    @IBOutlet weak var Activiter: UIActivityIndicatorView!
    
    var filteredCity: [String]!
    let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
    
    override func viewDidLoad() {
        SearchBar.delegate = self
        TableView.delegate = self
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        //send VC value to MainVC
        if segue.identifier == "ShowCityForecast"{
            
            let nav = segue.destinationViewController as! UINavigationController
            let DestVC = nav.topViewController as! MainViewController
            var indexPath:NSIndexPath
            indexPath = self.TableView.indexPathForSelectedRow!
            let selectedRow = indexPath.row

            //DestVC.btnCity.setTitle(filteredCity[selectedRow], forState: UIControlState.Normal)
            DestVC.cityName = filteredCity[selectedRow]
            DestVC.checkFlag = true
            self.Activiter.stopAnimating()
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        
        if filteredCity != nil{
            cell.textLabel?.text = filteredCity[indexPath.row]
        }
        return cell
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredCity == nil{
            return 0
        }
        else{
            return filteredCity.count
        }
        
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {

        self.Activiter.startAnimating()
        let selectedCity = filteredCity[indexPath.row]
        
        //save to coreData
        findCoredataItem(selectedCity)
    }
    
    
    //Save to CoreData
    func saveCoreDataItem(Item:String){
        let entity = NSEntityDescription.entityForName("CITY", inManagedObjectContext: managedContext)
        let city = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
        city.setValue(Item, forKey: "cityName")
        
        do{
            try managedContext.save()
        
        } catch let err as NSError{
            print(err.localizedDescription)
        }
    }
    
    
    func findCoredataItem(Item:String){
        
        let fetchRequest = NSFetchRequest(entityName: "CITY")
        fetchRequest.returnsObjectsAsFaults = false
        fetchRequest.predicate = NSPredicate(format: "cityName = %@", Item)
        do{
            let results = try managedContext.executeFetchRequest(fetchRequest)
            if(results.count > 0){
                //Alert.show("Attention", message: "Selected City have saved already", vc: self)
            }
            else{
                saveCoreDataItem(Item)
            }
            
        }catch{
            //Alert.show("Failed", message: "not able to search saved cities", vc: self)
        }
        
    
        
    }
    
    func searchBar(SearchBar: UISearchBar, textDidChange searchText: String) {
        if searchText.isEmpty{
            filteredCity = nil
        }
        else{
            
            if let cityEscaped = searchText.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLHostAllowedCharacterSet()){
                self.Activiter.startAnimating()
                filteredCity = GooglePlaceAutoCompeletService().getCitiesArray(cityEscaped)
                
            }
            
        }
        
        TableView.reloadData()
        self.Activiter.stopAnimating()
    }
    
    //dismiss keyboard
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.SearchBar.endEditing(true)
    }

}
