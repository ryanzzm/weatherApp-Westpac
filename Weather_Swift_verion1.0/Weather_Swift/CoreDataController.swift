//
//  CoreDataController.swift
//  Weather_Swift
//
//  Created by Ryan Zhang on 6/08/2016.
//  Copyright © 2016 Ryan Zhang. All rights reserved.
//

import UIKit
import CoreData

class CoreDataController: UITableViewController {

    var coreDataItem = [NSManagedObject]()
    
    @IBOutlet var Activator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(animated: Bool) {
        //fetch coreData
        let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
        
        let fetchRequest = NSFetchRequest(entityName: "CITY")
        do{
            let results = try managedContext.executeFetchRequest(fetchRequest)
            coreDataItem = results as! [NSManagedObject]
        }catch{
            Alert.show("Failed", message: "Can't display Saved Cities", vc: self)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowCityForecastFromCoreData"{
            var indexPath: NSIndexPath
            let DesVC = segue.destinationViewController as! MainViewController
            indexPath = self.tableView.indexPathForSelectedRow!
            let selectedItem = coreDataItem[indexPath.row]
            
            DesVC.cityName = selectedItem.valueForKey("cityName") as? String
            //DesVC.btnCity.setTitle((selectedItem.valueForKey("cityName") as? String), forState: UIControlState.Normal)
            DesVC.checkFlag = true
            Activator.stopAnimating()
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return coreDataItem.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        //load coreData
        let Cell = tableView.dequeueReusableCellWithIdentifier("cell")! as UITableViewCell
        let Item = coreDataItem[indexPath.row]
        Cell.textLabel?.text = Item.valueForKey("cityName") as? String
        return Cell
    }
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        Activator.startAnimating()
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        //delete coreData
        if editingStyle == .Delete{

            let managedContext = (UIApplication.sharedApplication().delegate as! AppDelegate).managedObjectContext
            
            managedContext.deleteObject(coreDataItem[indexPath.row])
            coreDataItem.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Automatic)
            tableView.reloadData()
        }
    }
    
}
