package forecastData;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Ref;
import java.util.ArrayList;
import java.util.List;

import forecastModel.Currently;
import forecastModel.Forecast;
import forecastModel.Hourly;
import forecastModel.Place;
import forecastModel.hourlyModel;
import forecastReference.Reference;

/**
 * Created by Ryan on 4/09/2016.
 */
public class ForecastJSONParser {
    public static Forecast getWeather(String data) {
        Forecast forecast = new Forecast();
        Place place = new Place();
        //Currently currently = new Currently();


        //connect data and model
        try {
            JSONObject jsonObject = new JSONObject(data);


            //parse data to forecastModel

            //1. current forecast data
            JSONObject currentObj = Reference.getObject("currently", jsonObject);
            forecast.currently.setTime(Reference.getInt("time", currentObj));
            forecast.currently.setSummary(Reference.getString("summary", currentObj));
            forecast.currently.setIcon(Reference.getString("icon", currentObj));
            forecast.currently.setTemperature(Reference.getDouble("temperature", currentObj));
            forecast.currently.setHumidity(Reference.getDouble("humidity", currentObj));
            forecast.currently.setPressure(Reference.getDouble("pressure", currentObj));
            forecast.currently.setWindSpeed(Reference.getDouble("windSpeed", currentObj));

            //2. hourly forecast data
            JSONObject hourlyObj = Reference.getObject("hourly", jsonObject);
            forecast.hourly.setIcon(Reference.getString("icon", hourlyObj));
            forecast.hourly.setSummary(Reference.getString("summary", hourlyObj));

            //2.1. hourly array data



//            JSONArray jsonArray = hourlyObj.getJSONArray("data");
//            List<hourlyModel> hourlyDatalist = new ArrayList<hourlyModel>();
//            String hrSummary;
//            long hrTime;
//            double hrTemp;
//            for (int i = 0; i < jsonArray.length(); i++){
//                 JSONObject jsonHourlyData = jsonArray.getJSONObject(i);
//                 hrTime = jsonHourlyData.getInt("time");
//                 hrSummary = jsonHourlyData.getString("summary");
//                 hrTemp = jsonHourlyData.getDouble("temperature");
//                 hourlyModel hourlyModel = new hourlyModel(hrTime, hrSummary, hrTemp);
//                 hourlyDatalist.add(hourlyModel);
//             }


            //forecast.hourly.data.setTime(Reference.getInt("time", jsonHourlyData));
            //forecast.hourly.data.setSummary(Reference.getString("summary", jsonHourlyData));

            return forecast;

        } catch (JSONException e) {
            e.printStackTrace();
            return null;
        }
    }
}
