package forecastReference;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Ryan on 3/09/2016.
 */
public class Reference {
    public static final String BASE_URL = "https://api.forecast.io/forecast/5417ce2f3c07bdac7bf5341fa01d4619/";
    public static final String GooglePlace_URL = "https://maps.googleapis.com/maps/api/place/autocomplete/json?input=";
    public static final String API = "&types=(cities)&language=en&key=AIzaSyDi4S9vNHDOyTAyXP_A_iXMdJdXSy7Sawo";

    public static JSONObject getObject(String tagName, JSONObject jsonObject) throws JSONException {
        JSONObject jObj = jsonObject.getJSONObject(tagName);
        return jObj;
    }

    public static String getString(String tagName, JSONObject jsonObject) throws JSONException {
        return jsonObject.getString(tagName);
    }

    public static Double getDouble(String tagName, JSONObject jsonObject) throws JSONException {
        return (double) jsonObject.getDouble(tagName);
    }

    public static Float getFloat(String tagName, JSONObject jsonObject) throws JSONException {
        return (float) jsonObject.getDouble(tagName);
    }

    public static int getInt(String tagName, JSONObject jsonObject) throws JSONException {
        return jsonObject.getInt(tagName);
    }

}


