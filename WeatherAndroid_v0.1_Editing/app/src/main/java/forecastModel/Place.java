package forecastModel;

/**
 * Created by Ryan on 4/09/2016.
 */
public class Place {
    private float latitude;
    private float lontitude;
    private String city;
    private String tumezone;


    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLontitude() {
        return lontitude;
    }

    public void setLontitude(float lontitude) {
        this.lontitude = lontitude;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getTumezone() {
        return tumezone;
    }

    public void setTumezone(String tumezone) {
        this.tumezone = tumezone;
    }
}
