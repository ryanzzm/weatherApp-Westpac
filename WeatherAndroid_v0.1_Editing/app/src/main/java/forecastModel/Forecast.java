package forecastModel;

/**
 * Created by Ryan on 3/09/2016.
 */
public class Forecast {
    public Currently currently = new Currently();
    public Hourly hourly = new Hourly();
    public Place place;
}
