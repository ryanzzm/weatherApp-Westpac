package forecastModel;

import java.lang.reflect.Array;
import java.util.Dictionary;
import java.util.List;

/**
 * Created by Ryan on 3/09/2016.
 */
public class Hourly {

    private String summary;
    private String icon;
    public hourlyModel data = new hourlyModel();

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }
}
