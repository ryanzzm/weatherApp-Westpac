package lifesgood.ryan.weatherandroid;

import android.app.ProgressDialog;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Date;

import forecastData.ForecastConnection;
import forecastData.ForecastJSONParser;
import forecastModel.Forecast;
import forecastModel.hourlyModel;

public class MainActivity extends AppCompatActivity {

    Forecast forecast = new Forecast();

    private ProgressDialog dialog;

    private TextView cityName;
    private TextView currentTemp;
    private ImageView currentIcon;
    private TextView currentTime;
    private TextView currentHum;
    private TextView currentWind;
    private TextView currentPre;
    private TextView currentSum;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        cityName = (TextView) findViewById(R.id.cityText);
        currentTemp = (TextView) findViewById(R.id.tempText);
        currentIcon = (ImageView) findViewById(R.id.iconImg);
        currentTime = (TextView) findViewById(R.id.updateNum);
        currentHum = (TextView) findViewById(R.id.humNum);
        currentWind = (TextView) findViewById(R.id.windNum);
        currentPre = (TextView) findViewById(R.id.preNum);
        currentSum = (TextView) findViewById(R.id.sum);

        renderForecastData("-33.8772,151.1049");

        //loading progress dialog box
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading...");
        dialog.show();

    }



    public void renderForecastData(String latANDlon) {
        WeatherTask weatherTask = new WeatherTask();
        weatherTask.execute(new String[]{latANDlon});

    }

    private class WeatherTask extends AsyncTask<String, Void, Forecast>{
        @Override
        protected Forecast doInBackground(String... params) {

            String data = ((new ForecastConnection()).getForecastData(params[0]));

            forecast = ForecastJSONParser.getWeather(data);

            //hourlyModel hourlymodel = new hourlyModel();

            Log.v("Data: ---", String.valueOf(forecast.currently.getTemperature()));
            Log.v("Data: 2---", forecast.hourly.getSummary());

            //Log.v("Data: 3---", forecast.hourly.data.get(0));

            return forecast;
        }

        @Override
        protected void onPostExecute(Forecast forecast) {
            super.onPostExecute(forecast);

            //convert unix to date
            DateFormat dateFormat = DateFormat.getDateTimeInstance();
            String currentTimeF = dateFormat.format(new Date(forecast.currently.getTime()*1000));
            currentTime.setText(currentTimeF);

            //format decimal with one digits
            //convert f to c
            double currentTempC = (forecast.currently.getTemperature()-32)/1.8;
            DecimalFormat decimalFormat = new DecimalFormat("#.#");
            String curremtTempFormat = decimalFormat.format(currentTempC);
            currentTemp.setText(curremtTempFormat + (char) 0x00B0 + "C");

            currentHum.setText(forecast.currently.getHumidity()*100 + "%");
            currentWind.setText(forecast.currently.getWindSpeed() + "km/h");
            currentPre.setText(forecast.currently.getPressure() + "");
            currentSum.setText(forecast.currently.getSummary());

            String currentIconF = forecast.currently.getIcon().replace("-","");

            int drawableResource = getResources().getIdentifier("drawable/" + currentIconF, null, getPackageName());


            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                Drawable drawable = getResources().getDrawable(drawableResource, getApplicationContext().getTheme());
                currentIcon.setImageDrawable(drawable);
            } else {
                Drawable drawable = getResources().getDrawable(drawableResource);
                currentIcon.setImageDrawable(drawable);
            }

            dialog.hide();
        }
    }

}
